<img src="https://code.freedombone.net/bashrc/freedombone/raw/buster/img/fbone_admin_screen.png?raw=true" width="60%"/>

Mainstream software is so broken and the organizations that develop it so untrustworthy that we are reaching a breaking point.

So you want to run your own internet services? Email, chat, VoIP, web sites, file synchronisation, wikis, blogs, social networks, media hosting, backups, VPN. Freedombone is a home server system which enables you to self-host all of these things.

You can run Freedombone on an old laptop, a single board computer, or use it to set up a mesh network in your local area.

XMPP MUC: **support@chat.freedombone.net**

Matrix room: **#fbone:matrix.freedombone.net**

See [the website](https://freedombone.net) for installation instructions and other information. It's also available via a Tor browser on http://dcxf7xthmh7dusiqgtj4sswksiifqhf4yloggpe4ucyo4zfapnbj4zyd.onion

<img src="https://code.freedombone.net/bashrc/freedombone/raw/buster/img/fbone_apps.jpg?raw=true" width="60%"/>
