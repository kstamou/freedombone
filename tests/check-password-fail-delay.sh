#!/bin/bash

if [ -f "/etc/login.defs" ];then
    VFAIL_DELAY=$1 # seconds

    RESULT=$(sed -e '/^#/d' -e '/^[ \t][ \t]*#/d' -e 's/#.*$//' -e '/^$/d' /etc/login.defs | grep FAIL_DELAY)
    if [ "$RESULT" ];then
        if [ $(echo "$RESULT" | awk '{printf $2}') -lt "${VFAIL_DELAY}" ];then
	    sed -i "s|FAIL_DELAY.*|FAIL_DELAY ${VFAIL_DELAY}|g" /etc/login.defs
            exit 1
        fi
    else
        echo "FAIL_DELAY ${VFAIL_DELAY}" >> /etc/login.defs
        exit 1
    fi
fi

if [ -f "/etc/pam.d/login" ];then
    VFAIL_DELAY=$(($1 * 1000000)) # microseconds

    RESULT=$(sed -e '/^#/d' -e '/^[ \t][ \t]*#/d' -e 's/#.*$//' -e '/^$/d' /etc/pam.d/login | grep 'pam_faildelay.so' | grep -P -o "\bdelay\s*=\s*\d+" | awk -F '=' '{print $2}')
    if [ "$RESULT" ];then
        if [ "$RESULT" -lt "${VFAIL_DELAY}" ];then
	    echo "login delay: $RESULT, minimum is ${VFAIL_DELAY}"
	    sed -i "s|delay=.*|delay=${VFAIL_DELAY}|g" /etc/pam.d/login
            exit 1
        fi
    else
	echo "auth       optional   pam_faildelay.so  delay=${VFAIL_DELAY}" >> /etc/pam.d/login
        exit 1
    fi
else
    exit 1
fi
