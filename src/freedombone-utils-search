#!/bin/bash
#  _____               _           _
# |   __|___ ___ ___ _| |___ _____| |_ ___ ___ ___
# |   __|  _| -_| -_| . | . |     | . | . |   | -_|
# |__|  |_| |___|___|___|___|_|_|_|___|___|_|_|___|
#
#                              Freedom in the Cloud
#
# Search with searx, for use with webadmin
#
# License
# =======
#
# Copyright © 2018-2020 Bob Mottram <bob@freedombone.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

SEARX_REPO="https://github.com/asciimoo/searx"
SEARX_COMMIT='a80a2d05d14290c57c3cfc59d373e597b1c1c4b5'
SEARX_PATH=/etc
SEARX_PORT=8888

function searx_set_default_background {
    bg_color=$(grep "WEBADMIN_BACKGROUND_COLOR=" "/root/${PROJECT_NAME}.cfg" | awk -F '=' '{print $2}')
    if [[ "$bg_color" != 'white' ]]; then
        searx_image_filename='searx_dark.png'
    else
        searx_image_filename='searx_light.png'
    fi
    if [ -f "$rootdir/root/${PROJECT_NAME}/img/backgrounds/${searx_image_filename}" ]; then
        cp "$rootdir/root/${PROJECT_NAME}/img/backgrounds/${searx_image_filename}" "$rootdir${SEARX_PATH}/searx/searx/static/themes/courgette/img/bg-body-index.jpg"
        if [ "$rootdir" ]; then
           chroot "$rootdir" chown -R searx:searx "${SEARX_PATH}/searx"
        else
           chown -R searx:searx "${SEARX_PATH}/searx"
        fi
    else
        if [ -f "$rootdir/home/$MY_USERNAME/${PROJECT_NAME}/img/backgrounds/${searx_image_filename}" ]; then
            cp "$rootdir/home/$MY_USERNAME/${PROJECT_NAME}/img/backgrounds/${searx_image_filename}" "$rootdir/etc/searx/searx/static/themes/courgette/img/bg-body-index.jpg"
            if [ "$rootdir" ]; then
                chroot "$rootdir" chown -R searx:searx "${SEARX_PATH}/searx"
            else
                chown -R searx:searx "${SEARX_PATH}/searx"
            fi
        fi
    fi

    # remove the github ribbon icon
    if [ -f "$rootdir/etc/searx/searx/static/themes/courgette/img/github_ribbon.png" ]; then
        mv "$rootdir/etc/searx/searx/static/themes/courgette/img/github_ribbon.png" "$rootdir${SEARX_PATH}/searx/searx/static/themes/courgette/img/github_ribbon.png.old"
    fi
}

function searx_set_base_url {
    settings_file="$rootdir${SEARX_PATH}/searx/searx/settings.yml"
    local_hostname=$(grep 'host-name' /etc/avahi/avahi-daemon.conf | awk -F '=' '{print $2}').local
    if [[ "$ONION_ONLY" != 'no' ]]; then
        if [ -f /var/lib/tor/hidden_service_webadmin/hostname ]; then
            local_hostname=$(cat /var/lib/tor/hidden_service_webadmin/hostname)
        fi
    fi
    searx_base_url="http://${local_hostname}/search"
    sed -i "s|base_url :.*|base_url : ${searx_base_url}|g" "$settings_file"
}

function create_searx_config {
    # shellcheck disable=SC2154
    settings_file="$rootdir${SEARX_PATH}/searx/searx/settings.yml"

    SEARX_SECRET_KEY=
    if [ -f "$rootdir$CONFIGURATION_FILE" ]; then
        if grep -q "SEARX_SECRET_KEY=" "$rootdir$CONFIGURATION_FILE"; then
            SEARX_SECRET_KEY=$(grep "SEARX_SECRET_KEY=" "$rootdir$CONFIGURATION_FILE" | head -n 1 | awk -F '=' '{print $2}')
        fi
    fi

    if [ ! "$SEARX_SECRET_KEY" ]; then
        SEARX_SECRET_KEY="$(create_random_string 30)"
        echo "SEARX_SECRET_KEY=$SEARX_SECRET_KEY" >> "$rootdir$CONFIGURATION_FILE"
    fi

    { echo 'general:';
      echo '    debug : False';
      echo '    instance_name : "Freedombone Metasearch"';
      echo '';
      echo 'search:';
      echo '    safe_search : 0 # Filter results. 0: None, 1: Moderate, 2: Strict';
      echo '    autocomplete : "" # Existing autocomplete backends: "dbpedia", "duckduckgo", "startpage", "wikipedia" - leave blank to turn it off by default';
      echo '    language : "en-US"';
      echo '';
      echo 'server:';
      echo "    port : ${SEARX_PORT}";
      echo '    bind_address : "127.0.0.1" # address to listen on';
      echo "    secret_key : \"${SEARX_SECRET_KEY}\"";
      echo "    base_url : ${searx_base_url}";
      echo '    image_proxy : True # Proxying image results through searx';
      echo '    http_protocol_version : "1.1"  # 1.0 and 1.1 are supported';
      echo '';
      echo 'ui:';
      echo '    static_path : "" # Custom static path - leave it blank if you didnt change';
      echo '    templates_path : "" # Custom templates path - leave it blank if you didnt change';
      echo '    themes_path : "" # Custom ui themes path';
      echo '    default_theme : courgette # ui theme';
      echo '    default_locale : "" # Default interface locale - leave blank to detect from browser information or use codes from the "locales" config section';
      echo '';
      echo 'outgoing: # communication with search engines';
      echo '    request_timeout : 10.0 # seconds';
      echo '    useragent_suffix : "" # suffix of searx_useragent, could contain informations like an email address to the administrator';
      echo '    pool_connections : 100 # Number of different hosts';
      echo '    pool_maxsize : 10 # Number of simultaneous requests by host';
      echo '    proxies :';
      echo '        http : socks5://127.0.0.1:9050';
      echo '';
      echo 'engines:';
      echo '  - name : arch linux wiki';
      echo '    engine : archlinux';
      echo '    categories : general';
      echo '    shortcut : al';
      echo '';
      echo '  - name : archive is';
      echo '    engine : xpath';
      echo '    search_url : https://archive.is/{query}';
      echo '    url_xpath : (//div[@class="TEXT-BLOCK"]/a)/@href';
      echo '    title_xpath : (//div[@class="TEXT-BLOCK"]/a)';
      echo '    content_xpath : //div[@class="TEXT-BLOCK"]/ul/li';
      echo '    categories : general';
      echo '    timeout : 7.0';
      echo '    disabled : True';
      echo '    shortcut : ai';
      echo '';
      echo '  - name : base';
      echo '    engine : base';
      echo '    shortcut : bs';
      echo '';
      echo '  - name : wikipedia';
      echo '    engine : wikipedia';
      echo '    shortcut : wp';
      echo '    categories : general';
      echo '    base_url : "https://{language}.wikipedia.org/"';
      echo '';
      echo '  - name : bitbucket';
      echo '    engine : xpath';
      echo '    paging : True';
      echo '    search_url : https://bitbucket.org/repo/all/{pageno}?name={query}';
      echo '    url_xpath : //article[@class="repo-summary"]//a[@class="repo-link"]/@href';
      echo '    title_xpath : //article[@class="repo-summary"]//a[@class="repo-link"]';
      echo '    content_xpath : //article[@class="repo-summary"]/p';
      echo '    categories : code';
      echo '    timeout : 4.0';
      echo '    disabled : True';
      echo '    shortcut : bb';
      echo '';
      echo '  - name : ccc-tv';
      echo '    engine : xpath';
      echo '    paging : False';
      echo '    search_url : https://media.ccc.de/search/?q={query}';
      echo '    url_xpath : //div[@class="caption"]/h3/a/@href';
      echo '    title_xpath : //div[@class="caption"]/h3/a/text()';
      echo '    content_xpath : //div[@class="caption"]/h4/@title';
      echo '    categories : videos';
      echo '    shortcut : c3tv';
      echo '';
      echo '  - name : crossref';
      echo '    engine : json_engine';
      echo '    paging : True';
      echo '    search_url : http://search.crossref.org/dois?q={query}&page={pageno}';
      echo '    url_query : doi';
      echo '    title_query : title';
      echo '    content_query : fullCitation';
      echo '    categories : science';
      echo '    shortcut : cr';
      echo '';
      echo '  - name : currency';
      echo '    engine : currency_convert';
      echo '    categories : general';
      echo '    shortcut : cc';
      echo '';
      echo '  - name : deezer';
      echo '    engine : deezer';
      echo '    shortcut : dz';
      echo '';
      echo '  - name : deviantart';
      echo '    engine : deviantart';
      echo '    shortcut : da';
      echo '    timeout: 3.0';
      echo '    disabled : True';
      echo '';
      echo '  - name : ddg definitions';
      echo '    engine : duckduckgo_definitions';
      echo '    shortcut : ddd';
      echo '    weight : 2';
      echo '    disabled : True';
      echo '';
      echo '  - name : digbt';
      echo '    engine : digbt';
      echo '    shortcut : dbt';
      echo '    timeout : 6.0';
      echo '    disabled : True';
      echo '';
      echo '  - name : erowid';
      echo '    engine : xpath';
      echo '    paging : True';
      echo '    first_page_num : 0';
      echo '    page_size : 30';
      echo '    search_url : https://www.erowid.org/search.php?q={query}&s={pageno}';
      echo '    url_xpath : //dl[@class="results-list"]/dt[@class="result-title"]/a/@href';
      echo '    title_xpath : //dl[@class="results-list"]/dt[@class="result-title"]/a/text()';
      echo '    content_xpath : //dl[@class="results-list"]/dd[@class="result-details"]';
      echo '    categories : general';
      echo '    shortcut : ew';
      echo '    disabled : True';
      echo '';
      echo '  - name : wikidata';
      echo '    engine : wikidata';
      echo '    shortcut : wd';
      echo '    weight : 2';
      echo '';
      echo '  - name : duckduckgo';
      echo '    engine : duckduckgo';
      echo '    shortcut : ddg';
      echo '    categories : general';
      echo '';
      echo '  - name : etymonline';
      echo '    engine : xpath';
      echo '    paging : True';
      echo '    search_url : http://etymonline.com/?search={query}&p={pageno}';
      echo '    url_xpath : //dt/a[1]/@href';
      echo '    title_xpath : //dt';
      echo '    content_xpath : //dd';
      echo '    suggestion_xpath : //a[@class="crossreference"]';
      echo '    first_page_num : 0';
      echo '    shortcut : et';
      echo '    disabled : True';
      echo '';
      echo '  - name : 1x';
      echo '    engine : www1x';
      echo '    shortcut : 1x';
      echo '    disabled : True';
      echo '';
      echo '  - name : fdroid';
      echo '    engine : fdroid';
      echo '    shortcut : fd';
      echo '    disabled : True';
      echo '';
      echo '  - name : flickr';
      echo '    categories : images';
      echo '    shortcut : fl';
      echo '# You can use the engine using the official stable API, but you need an API key';
      echo '# See : https://www.flickr.com/services/apps/create/';
      echo '#    engine : flickr';
      echo "#    api_key: 'apikey' # required!";
      echo '# Or you can use the html non-stable engine, activated by default';
      echo '    engine : flickr_noapi';
      echo '';
      echo '  - name : frinkiac';
      echo '    engine : frinkiac';
      echo '    shortcut : frk';
      echo '    disabled : True';
      echo '';
      echo '  - name : gigablast';
      echo '    engine : gigablast';
      echo '    shortcut : gb';
      echo '    timeout : 3.0';
      echo '    disabled: True';
      echo '';
      echo '  - name : gitlab';
      echo '    engine : json_engine';
      echo '    paging : True';
      echo '    search_url : https://gitlab.com/api/v4/projects?search={query}&page={pageno}';
      echo '    url_query : web_url';
      echo '    title_query : name_with_namespace';
      echo '    content_query : description';
      echo '    page_size : 20';
      echo '    categories : code';
      echo '    shortcut : gl';
      echo '    timeout : 10.0';
      echo '    disabled : False';
      echo '';
      echo '  - name : github';
      echo '    engine : github';
      echo '    shortcut : gh';
      echo '    categories : code';
      echo '';
      echo '  - name : geektimes';
      echo '    engine : xpath';
      echo '    paging : True';
      echo '    search_url : https://geektimes.ru/search/page{pageno}/?q={query}';
      echo '    url_xpath : //div[@class="search_results"]//a[@class="post__title_link"]/@href';
      echo '    title_xpath : //div[@class="search_results"]//a[@class="post__title_link"]';
      echo '    content_xpath : //div[@class="search_results"]//div[contains(@class, "content")]';
      echo '    categories : code';
      echo '    timeout : 4.0';
      echo '    disabled : True';
      echo '    shortcut : gt';
      echo '';
      echo '  - name : habrahabr';
      echo '    engine : xpath';
      echo '    paging : True';
      echo '    search_url : https://habrahabr.ru/search/page{pageno}/?q={query}';
      echo '    url_xpath : //div[@class="search_results"]//a[contains(@class, "post__title_link")]/@href';
      echo '    title_xpath : //div[@class="search_results"]//a[contains(@class, "post__title_link")]';
      echo '    content_xpath : //div[@class="search_results"]//div[contains(@class, "content")]';
      echo '    categories : code';
      echo '    timeout : 4.0';
      echo '    disabled : True';
      echo '    shortcut : habr';
      echo '';
      echo '  - name : hoogle';
      echo '    engine : json_engine';
      echo '    paging : True';
      echo '    search_url : https://www.haskell.org/hoogle/?mode=json&hoogle={query}&start={pageno}';
      echo '    results_query : results';
      echo '    url_query : location';
      echo '    title_query : self';
      echo '    content_query : docs';
      echo '    page_size : 20';
      echo '    categories : code';
      echo '    shortcut : ho';
      echo '';
      echo '  - name : ina';
      echo '    engine : ina';
      echo '    shortcut : in';
      echo '    timeout : 6.0';
      echo '    disabled : True';
      echo '';
      echo '  - name: kickass';
      echo '    engine : kickass';
      echo '    shortcut : kc';
      echo '    timeout : 4.0';
      echo '    disabled : True';
      echo '';
      echo '  - name : library genesis';
      echo '    engine : xpath';
      echo '    search_url : http://libgen.io/search.php?req={query}';
      echo '    url_xpath : //a[contains(@href,"bookfi.net")]/@href';
      echo '    title_xpath : //a[contains(@href,"book/")]/text()[1]';
      echo '    content_xpath : //td/a[1][contains(@href,"=author")]/text()';
      echo '    categories : general';
      echo '    timeout : 7.0';
      echo '    disabled : True';
      echo '    shortcut : lg';
      echo '';
      echo '  - name : lobste.rs';
      echo '    engine : xpath';
      echo '    search_url : https://lobste.rs/search?utf8=%E2%9C%93&q={query}&what=stories&order=relevance';
      echo '    results_xpath : //li[contains(@class, "story")]';
      echo '    url_xpath : .//span[@class="link"]/a/@href';
      echo '    title_xpath : .//span[@class="link"]/a';
      echo '    content_xpath : .//a[@class="domain"]';
      echo '    categories : code';
      echo '    shortcut : lo';
      echo '';
      echo '  - name : mixcloud';
      echo '    engine : mixcloud';
      echo '    shortcut : mc';
      echo '';
      echo '  - name : nyaa';
      echo '    engine : nyaa';
      echo '    shortcut : nt';
      echo '    disabled : True';
      echo '';
      echo '  - name : openstreetmap';
      echo '    engine : openstreetmap';
      echo '    shortcut : osm';
      echo '';
      echo '  - name : openrepos';
      echo '    engine : xpath';
      echo '    paging : True';
      echo '    search_url : https://openrepos.net/search/node/{query}?page={pageno}';
      echo '    url_xpath : //li[@class="search-result"]//h3[@class="title"]/a/@href';
      echo '    title_xpath : //li[@class="search-result"]//h3[@class="title"]/a';
      echo '    content_xpath : //li[@class="search-result"]//div[@class="search-snippet-info"]//p[@class="search-snippet"]';
      echo '    categories : files';
      echo '    timeout : 4.0';
      echo '    disabled : True';
      echo '    shortcut : or';
      echo '';
      echo '  - name : pdbe';
      echo '    engine : pdbe';
      echo '    shortcut : pdb';
      echo '';
      echo '  - name : photon';
      echo '    engine : photon';
      echo '    shortcut : ph';
      echo '';
      echo '  - name : piratebay';
      echo '    engine : piratebay';
      echo '    shortcut : tpb';
      echo '    url: https://pirateproxy.red/';
      echo '    timeout : 3.0';
      echo '';
      echo '  - name : qwant';
      echo '    engine : qwant';
      echo '    shortcut : qw';
      echo '    categories : general';
      echo '    disabled : True';
      echo '';
      echo '  - name : qwant images';
      echo '    engine : qwant';
      echo '    shortcut : qwi';
      echo '    categories : images';
      echo '';
      echo '  - name : qwant news';
      echo '    engine : qwant';
      echo '    shortcut : qwn';
      echo '    categories : news';
      echo '';
      echo '  - name : qwant social';
      echo '    engine : qwant';
      echo '    shortcut : qws';
      echo '    categories : social media';
      echo '';
      echo '  - name : reddit';
      echo '    engine : reddit';
      echo '    shortcut : re';
      echo '    page_size : 25';
      echo '    timeout : 10.0';
      echo '    disabled : True';
      echo '';
      echo '  - name : scanr structures';
      echo '    shortcut: scs';
      echo '    engine : scanr_structures';
      echo '    disabled : True';
      echo '';
      echo '  - name : soundcloud';
      echo '    engine : soundcloud';
      echo '    shortcut : sc';
      echo '';
      echo '  - name : stackoverflow';
      echo '    engine : stackoverflow';
      echo '    shortcut : st';
      echo '';
      echo '  - name : searchcode doc';
      echo '    engine : searchcode_doc';
      echo '    shortcut : scd';
      echo '';
      echo '  - name : searchcode code';
      echo '    engine : searchcode_code';
      echo '    shortcut : scc';
      echo '    disabled : True';
      echo '';
      echo '  - name : framalibre';
      echo '    engine : framalibre';
      echo '    shortcut : frl';
      echo '    disabled : True';
      echo '';
      echo '  - name : semantic scholar';
      echo '    engine : xpath';
      echo '    paging : True';
      echo '    search_url : https://www.semanticscholar.org/search?q={query}&sort=relevance&page={pageno}&ae=false';
      echo '    results_xpath : //article';
      echo '    url_xpath : .//div[@class="search-result-title"]/a/@href';
      echo '    title_xpath : .//div[@class="search-result-title"]/a';
      echo '    content_xpath : .//div[@class="search-result-abstract"]';
      echo '    shortcut : se';
      echo '    categories : science';
      echo '';
      echo '  - name : spotify';
      echo '    engine : spotify';
      echo '    shortcut : stf';
      echo '';
      echo '  - name : subtitleseeker';
      echo '    engine : subtitleseeker';
      echo '    shortcut : ss';
      echo '# The language is an option. You can put any language written in english';
      echo '# Examples : English, French, German, Hungarian, Chinese...';
      echo '#    language : English';
      echo '';
      echo '  - name : startpage';
      echo '    engine : startpage';
      echo '    shortcut : sp';
      echo '    timeout : 6.0';
      echo '    disabled : True';
      echo '';
      echo '  - name : ixquick';
      echo '    engine : startpage';
      echo "    base_url : 'https://www.ixquick.eu/'";
      echo "    search_url : 'https://www.ixquick.eu/do/search'";
      echo '    shortcut : iq';
      echo '    timeout : 6.0';
      echo '';
      echo '  - name : swisscows';
      echo '    engine : swisscows';
      echo '    shortcut : sw';
      echo '    disabled : True';
      echo '';
      echo '  - name : tokyotoshokan';
      echo '    engine : tokyotoshokan';
      echo '    shortcut : tt';
      echo '    timeout : 6.0';
      echo '    disabled : True';
      echo '';
      echo '  - name : twitter';
      echo '    engine : twitter';
      echo '    shortcut : tw';
      echo '';
      echo '  - name : yandex';
      echo '    engine : yandex';
      echo '    shortcut : yn';
      echo '    disabled : True';
      echo '';
      echo '  - name : youtube';
      echo '    shortcut : yt';
      echo '    # You can use the engine using the official stable API, but you need an API key';
      echo '    # See : https://console.developers.google.com/project';
      echo '    #    engine : youtube_api';
      echo '    #    api_key: apikey # required!';
      echo '    # Or you can use the html non-stable engine, activated by default';
      echo '    engine : youtube_noapi';
      echo '';
      echo '  - name : dailymotion';
      echo '    engine : dailymotion';
      echo '    shortcut : dm';
      echo '';
      echo '  - name : vimeo';
      echo '    engine : vimeo';
      echo '    shortcut : vm';
      echo '';
      echo '  - name : wolframalpha';
      echo '    shortcut : wa';
      echo '    # You can use the engine using the official stable API, but you need an API key';
      echo '    # See : http://products.wolframalpha.com/api/';
      echo '    # engine : wolframalpha_api';
      echo '    # api_key: '' # required!';
      echo '    engine : wolframalpha_noapi';
      echo '    timeout: 6.0';
      echo '    categories : science';
      echo '';
      echo '  - name : seedpeer';
      echo '    engine : seedpeer';
      echo '    shortcut: speu'
      echo '    categories: files, music, videos'
      echo '    disabled: True';
      echo '';
      echo '  - name : dictzone';
      echo '    engine : dictzone';
      echo '    shortcut : dc';
      echo '';
      echo '  - name : mymemory translated';
      echo '    engine : translated';
      echo '    shortcut : tl';
      echo '    timeout : 5.0';
      echo '    disabled : True';
      echo '    # You can use without an API key, but you are limited to 1000 words/day';
      echo '    # See : http://mymemory.translated.net/doc/usagelimits.php';
      echo '    # api_key : ""';
      echo '';
      echo '  - name : voat';
      echo '    engine: xpath';
      echo '    shortcut: vo';
      echo '    categories: social media';
      echo '    search_url : https://voat.co/search?q={query}';
      echo '    url_xpath : //p[contains(@class, "title")]/a/@href';
      echo '    title_xpath : //p[contains(@class, "title")]/a';
      echo '    content_xpath : //span[@class="domain"]';
      echo '    timeout : 10.0';
      echo '    disabled : True';
      echo '';
      echo '  - name : 1337x';
      echo '    engine : 1337x';
      echo '    shortcut : 1337x';
      echo '    disabled : True';
      echo '';
      echo 'locales:';
      echo '    en : English';
      echo '    ar : العَرَبِيَّة (Arabic)';
      echo '    bg : Български (Bulgarian)';
      echo '    ca : Català (Catalan)';
      echo '    cs : Čeština (Czech)';
      echo '    cy : Cymraeg (Welsh)';
      echo '    da : Dansk (Danish)';
      echo '    de : Deutsch (German)';
      echo '    el_GR : Ελληνικά (Greek_Greece)';
      echo '    eo : Esperanto (Esperanto)';
      echo '    es : Español (Spanish)';
      echo '    eu : Euskara (Basque)';
      echo '    fa_IR : (fārsī) فارسى (Persian)';
      echo '    fi : Suomi (Finnish)';
      echo '    fil : Wikang Filipino (Filipino)';
      echo '    fr : Français (French)';
      echo '    gl : Galego (Galician)';
      echo '    he : עברית (Hebrew)';
      echo '    hr : Hrvatski (Croatian)';
      echo '    hu : Magyar (Hungarian)';
      echo '    it : Italiano (Italian)';
      echo '    ja : 日本語 (Japanese)';
      echo '    nl : Nederlands (Dutch)';
      echo '    nl_BE : Vlaams (Dutch_Belgium)';
      echo '    pl : Polski (Polish)';
      echo '    pt : Português (Portuguese)';
      echo '    pt_BR : Português (Portuguese_Brazil)';
      echo '    ro : Română (Romanian)';
      echo '    ru : Русский (Russian)';
      echo '    sk : Slovenčina (Slovak)';
      echo '    sl : Slovenski (Slovene)';
      echo '    sr : српски (Serbian)';
      echo '    sv : Svenska (Swedish)';
      echo '    te : తెలుగు (telugu)';
      echo '    tr : Türkçe (Turkish)';
      echo '    uk : українська мова (Ukrainian)';
      echo '    vi : tiếng việt (㗂越)';
      echo '    zh : 中文 (Chinese)';
      echo '    zh_TW : 國語 (Taiwanese Mandarin)';
      echo '';
      echo 'doi_resolvers :';
      echo "    oadoi.org : 'https://oadoi.org/'";
      echo "    doi.org : 'https://doi.org/'";
      echo "    doai.io : 'http://doai.io/'";
      echo '';
      echo "default_doi_resolver : 'oadoi.org'"; } > "$settings_file"

    searx_set_base_url
}

function webadmin_upgrade_search {
    if grep -q 'python ' /etc/systemd/system/searx.service; then
	echo 'Upgrading searx to use python3'
	# shellcheck disable=SC2086
	$INSTALL_PACKAGES python3-pygments python3-werkzeug python3-flask python3-flask-babel python3-lxml
	sed -i 's|python |python3 |g' /etc/systemd/system/searx.service
	systemctl daemon-reload
	systemctl restart searx
    fi

    CURR_SEARX_COMMIT=$(grep "SEARX_COMMIT=" "$rootdir$CONFIGURATION_FILE" | head -n 1 | awk -F '=' '{print $2}')
    if [[ "$CURR_SEARX_COMMIT" == "$SEARX_COMMIT" ]]; then
        searx_set_base_url
        systemctl restart searx
        return
    fi

    settings_file="$rootdir${SEARX_PATH}/searx/searx/settings.yml"
    background_image="$rootdir/etc/searx/searx/static/themes/courgette/img/bg-body-index.jpg"

    # save the background image
    if [ -f "${background_image}" ]; then
        cp "${background_image}" "${background_image}.prev"
    fi

    # save the settings
    cp "${settings_file}" "${settings_file}.prev"

    # do the upgrade
    cat >> "$rootdir/usr/bin/upgrade_searx" <<EOF
cd $SEARX_PATH/searx || exit 1
git stash
git pull
./manage.sh update_packages
EOF
    chmod +x "$rootdir/usr/bin/upgrade_searx"
    if [ "$rootdir" ]; then
        chroot "$rootdir" /bin/bash /usr/bin/upgrade_searx
        chroot "$rootdir" rm /usr/bin/upgrade_searx
    else
        /bin/bash /usr/bin/upgrade_searx
        rm /usr/bin/upgrade_searx
    fi

    # restore the background image
    if [ -f "${background_image}.prev" ]; then
        cp "${background_image}.prev" "${background_image}"
        if [ "$rootdir" ]; then
            chroot "$rootdir" chown -R searx:searx "${SEARX_PATH}/searx"
        else
            chown -R searx:searx "${SEARX_PATH}/searx"
        fi
    fi

    # restore the settings
    if [ -f "${settings_file}.prev" ]; then
        cp "${settings_file}.prev" "${settings_file}"
        if [ "$rootdir" ]; then
            chroot "$rootdir" chown -R searx:searx "${SEARX_PATH}/searx"
        else
            chown -R searx:searx "${SEARX_PATH}/searx"
        fi
    fi

    # remove the github ribbon icon
    if [ -f "$rootdir/etc/searx/searx/static/themes/courgette/img/github_ribbon.png" ]; then
        mv "$rootdir/etc/searx/searx/static/themes/courgette/img/github_ribbon.png" "$rootdir/etc/searx/searx/static/themes/courgette/img/github_ribbon.png.old"
    fi

    sed -i "s|SEARX_COMMIT=.*|SEARX_COMMIT=$SEARX_COMMIT|g" "$rootdir$CONFIGURATION_FILE"
    searx_set_base_url
    systemctl restart searx
}

function webadmin_install_search {
    if grep -q "SEARX_COMMIT=" "$rootdir$CONFIGURATION_FILE"; then
        webadmin_upgrade_search
        return
    fi

    if [ "$rootdir" ]; then
        # remove downloaded packages
        # shellcheck disable=SC2086
        chroot "$rootdir" $REMOVE_UNUSED_PACKAGES
        # shellcheck disable=SC2086
        chroot "$rootdir" $CLEAN_PACKAGES

        # shellcheck disable=SC2086
        chroot "$rootdir" $INSTALL_PACKAGES python-pip libyaml-dev python-werkzeug python-babel python-lxml
        # shellcheck disable=SC2086
        chroot "$rootdir" $INSTALL_PACKAGES python-requests python-wheel python-libxml2 libffi-dev
        # shellcheck disable=SC2086
	chroot "$rootdir" $INSTALL_PACKAGES python3-pygments python3-werkzeug python3-flask python3-flask-babel python3-lxml
        # shellcheck disable=SC2086
        chroot "$rootdir" $INSTALL_PACKAGES git build-essential libxslt-dev python-dev python-virtualenv zlib1g-dev uwsgi uwsgi-plugin-python imagemagick
        # shellcheck disable=SC2086
        chroot "$rootdir" $INSTALL_PACKAGES apache2-utils python-setuptools

        # remove downloaded packages
        # shellcheck disable=SC2086
        chroot "$rootdir" $REMOVE_UNUSED_PACKAGES
        # shellcheck disable=SC2086
        chroot "$rootdir" $CLEAN_PACKAGES

        # shellcheck disable=SC2086
        chroot "$rootdir" $INSTALL_PACKAGES python-service-identity python-ndg-httpsclient
        # shellcheck disable=SC2086
        chroot "$rootdir" $REMOVE_PACKAGES_PURGE apache2-bin*

        # remove downloaded packages
        # shellcheck disable=SC2086
        chroot "$rootdir" $REMOVE_UNUSED_PACKAGES
        # shellcheck disable=SC2086
        chroot "$rootdir" $CLEAN_PACKAGES
    else
        $REMOVE_UNUSED_PACKAGES
        # shellcheck disable=SC2086
        $CLEAN_PACKAGES

        # shellcheck disable=SC2086
        $INSTALL_PACKAGES python-pip libyaml-dev python-werkzeug python-babel python-lxml python-yaml
	# shellcheck disable=SC2086
	$INSTALL_PACKAGES python3-pygments python3-werkzeug python3-flask python3-flask-babel python3-lxml
        # shellcheck disable=SC2086
        $INSTALL_PACKAGES python-requests python-wheel python-libxml2 libffi-dev
        # shellcheck disable=SC2086
        $INSTALL_PACKAGES git build-essential libxslt-dev python-dev python-virtualenv zlib1g-dev uwsgi uwsgi-plugin-python imagemagick
        # shellcheck disable=SC2086
        $INSTALL_PACKAGES apache2-utils python-setuptools

        # remove downloaded packages
        # shellcheck disable=SC2086
        $REMOVE_UNUSED_PACKAGES
        # shellcheck disable=SC2086
        $CLEAN_PACKAGES

        # shellcheck disable=SC2086
        $INSTALL_PACKAGES python-service-identity python-ndg-httpsclient
        # shellcheck disable=SC2086
        $REMOVE_PACKAGES_PURGE apache2-bin*

        # remove downloaded packages
        # shellcheck disable=SC2086
        $REMOVE_UNUSED_PACKAGES
        # shellcheck disable=SC2086
        $CLEAN_PACKAGES
    fi

    cat >> "$rootdir/usr/bin/install_searx" <<EOF
#!/bin/bash

#pip install --upgrade pip==19.3.1

#if ! pip install pyyaml; then
#    echo $'Failed to install pyyaml'
#    exit 46
#fi

if ! pip install flask --upgrade; then
    echo $'Failed to install flask'
    exit 88
fi

if ! pip install flask_restless --upgrade; then
    echo $'Failed to install flask_restless'
    exit 54
fi

if ! pip install flask_babel --upgrade; then
    echo $'Failed to install flask_babel'
    exit 63
fi

#if ! pip install requests --upgrade; then
#    echo $'Failed to install requests'
#    exit 35
#fi

if ! pip install pygments --upgrade; then
    echo $'Failed to install pygments'
    exit 35
fi

#pip install --upgrade --force "pynacl>=1.2.1"

if [ -d "${SEARX_PATH}/searx" ]; then
   rm -rf "${SEARX_PATH}/searx"
fi

if ! git clone $SEARX_REPO ${SEARX_PATH}/searx; then
    exit 35
fi

cd $SEARX_PATH/searx || exit 26
git checkout $SEARX_COMMIT -b $SEARX_COMMIT

useradd -d ${SEARX_PATH}/searx/ -s /bin/false searx
adduser searx debian-tor
#if ! pip install -r requirements.txt; then
#    echo 'Failed to install searx requirements'
#    exit 72
#fi
EOF
    if [ ! -f "$rootdir/usr/bin/install_searx" ]; then
        echo "$rootdir/usr/bin/install_searx not found"
        return
    fi
    chmod +x "$rootdir/usr/bin/install_searx"
    if [ "$rootdir" ]; then
        chroot "$rootdir" /bin/bash /usr/bin/install_searx
        chroot "$rootdir" rm /usr/bin/install_searx
    else
        /bin/bash /usr/bin/install_searx
        rm /usr/bin/install_searx
    fi

    { echo '[Unit]';
      echo 'Description=Searx (search engine)';
      echo 'After=syslog.target';
      echo 'After=network.target';
      echo '';
      echo '[Service]';
      echo 'Type=simple';
      echo 'User=searx';
      echo 'Group=searx';
      echo "WorkingDirectory=${SEARX_PATH}/searx";
      echo "ExecStart=/usr/bin/python3 ${SEARX_PATH}/searx/searx/webapp.py";
      echo 'Restart=always';
      echo 'Environment="USER=searx"';
      echo 'PrivateTmp=true';
      echo 'PrivateDevices=false';
      echo 'NoNewPrivileges=true';
      echo 'CapabilityBoundingSet=~CAP_SYS_ADMIN';
      echo '';
      echo '[Install]';
      echo 'WantedBy=multi-user.target'; } > "$rootdir/etc/systemd/system/searx.service"

    create_searx_config
    searx_set_default_background

    if [ ! -d "${rootdir}${SEARX_PATH}/searx" ]; then
        echo $'Failed to install search'
        return
    fi

    if [ "$rootdir" ]; then
        chroot "$rootdir" chown -R searx:searx "${SEARX_PATH}/searx"
        chroot "$rootdir" systemctl enable searx.service
    else
        chown -R searx:searx "${SEARX_PATH}/searx"
        systemctl enable searx.service
        systemctl daemon-reload
        systemctl start searx.service
    fi

    if ! grep -q "SEARX_COMMIT=" "$rootdir$CONFIGURATION_FILE"; then
        echo "SEARX_COMMIT=$SEARX_COMMIT" >> "$rootdir$CONFIGURATION_FILE"
    else
        sed -i "s|SEARX_COMMIT=.*|SEARX_COMMIT=$SEARX_COMMIT|g" "$rootdir$CONFIGURATION_FILE"
    fi
}

# NOTE: deliberately no exit 0
