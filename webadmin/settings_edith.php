<?php

//  _____               _           _
// |   __|___ ___ ___ _| |___ _____| |_ ___ ___ ___
// |   __|  _| -_| -_| . | . |     | . | . |   | -_|
// |__|  |_| |___|___|___|___|_|_|_|___|___|_|_|___|
//
//                              Freedom in the Cloud
//
// Edith settings menu
//
// License
// =======
//
// Copyright (C) 2018-2019 Bob Mottram <bob@freedombone.net>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

include dirname(__FILE__)."/common.php";

$output_filename = "app_edith.html";

if (php_sapi_name()!=='fpm-fcgi') exit('php script must be run from the web interface');

if (isset($_POST['submitenablepassword'])) {
    if(filter_string('edith_password',512)) {
        $password = trim(htmlspecialchars($_POST['edith_password']));
        $password_enabled = '0';
        if (preg_match('/^[a-z\A-Z\d_]{8,512}$/', $password)) {
            $password_enabled = '1';
        }

        $settings_file = fopen(".appsettings.txt", "w") or die("Unable to write to appsettings file");
        fwrite($settings_file, "edith,enablepassword,".$password);
        fclose($settings_file);
    }
}

$htmlfile = fopen("$output_filename", "r") or die("Unable to open $output_filename");
echo fread($htmlfile,filesize("$output_filename"));
fclose($htmlfile);

?>
